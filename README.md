Given a file like 
```
Header
	Some stuff
	blah blah blah
Another header
	More lines
```
this program will let you fuzzy search through the titles then print the contents of the one you choose to stdout.
Use the -l option to just list titles instead.
Use the -q option to print only the indented lines rather than title + lines.
It reads from a file if you pass one as an argument or from stdin.
