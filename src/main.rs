extern crate fuzzy_finder;

use fuzzy_finder::{
	item::Item,
	FuzzyFinder
	};
use std::{
	collections::HashMap,
	env,
	fs,
	io::{
		self,
		BufRead,
		Read,
		Write
		},
	process
	};

struct Prefs {
	list: bool,
	quiet: bool,
	file: String,
}

// Make items vector
fn itemizer(headers: Vec<String>) -> Vec<Item<String>> {
	let mut items = Vec::new();
	for header in headers {
		let item = Item::new(header.to_string(), header.to_string());
		items.push(item);
	}
	items
}

// Split into hashmap
fn mapper(lines: Vec<u8>) -> HashMap<String,String> {
    //let mut lines = Cursor::new(a).lines().peekable();
    let mut lines = lines.lines().peekable();
    let mut map = HashMap::new();
    while let Some(Ok(line)) = lines.next() {
        if line.starts_with("\t") || line.is_empty() { unreachable!() }
        let header = line;
        let mut content = String::new();
        while let Some(Ok(line)) = lines.next_if(|item| item.as_ref().unwrap().starts_with("\t") || item.as_ref().unwrap().is_empty()) {
           content.push_str(line.strip_prefix("\t").unwrap_or("")); 
           content.push('\n');
        }  
	match content.strip_suffix('\n') {
		Some(sp) => content = sp.to_string(),
		None => ()
	}
        map.insert(header,content);
    }
    map
}

// Argument parsing
fn parse_args() -> Prefs {
	let args = env::args().skip(1);
	let mut prefs = Prefs { 
			list: false, 
			quiet: false,
			file: String::new(),
			};

	for arg in args {
		match arg.strip_prefix('-') {
			Some("h") |
			Some("-help") => {
				println!(
					"Tabby parses tab-indented files.\n\
					Use -l to print all headers instead of invoking the fuzzy finder UI.\n\
					Use -q to skip printing the selected item's name.\n\
					Data is read from a pipe or from a file passed as an argument."
					);
				process::exit(0);
			},
			Some(opt) => {
				for c in opt.chars() {
					match c {
						'l' => prefs.list = true,
						'q' => prefs.quiet = true,
						_ => {
							eprintln!("{c}: Unrecognized option.");
							process::exit(1);
						}
					}
				}
			},
			None => {
				if prefs.file.is_empty() {
					prefs.file.push_str(&arg);
				} else {
					eprintln!("Too many arguments! Using the first one.");
				}
			}
		}
	}
	prefs
}

fn main() {
	let prefs = parse_args();
	let data: Vec<u8> = if prefs.file.is_empty() {
			let mut vec = Vec::new();
			let mut stdin = io::stdin().lock();
			stdin.read_to_end(&mut vec).expect("Failed to read from stdin");
			vec
		} else {
			fs::read(&prefs.file).expect(&format!("Failed to read file: {}", prefs.file))
		};
	let main_map = mapper(data);		
	let headers: Vec<String> = main_map.keys().map(|item| item.to_string()).collect();	

	if prefs.list {
		println!("{}", headers.join("\n"));
	} else {
		let items = itemizer(headers); 
		let choice = FuzzyFinder::find(items, 10).unwrap().unwrap_or_else(|| process::exit(0));
		// Clear fuzzy finder UI 
		print!("\x1B[0m\x1B[G\x1B[10A\x1B[J");
		io::stdout().flush().unwrap();

		match main_map.get(&choice) {
			Some(value) => {
				if prefs.quiet {
					println!("{value}")
				} else {
					println!("\x1B[1m{choice}\x1B[0m\n\t{}", value.replace("\n","\n\t"))
				}
			},
			None => println!("{choice}")
		}
	}
}
